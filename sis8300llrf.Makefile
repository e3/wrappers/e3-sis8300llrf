#
#  Copyright (c) 2018 - 2021  European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
#
#
# Author  : gabrielfedel
# 			joaopaulomartins
#           Jeong Han Lee
# email   : gabrielfedel@ess.eu
# 			joaopaulomartins@esss.se
#           jeonghan.lee@gmail.com

where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile



# print cc1plus: warning: unrecognized command line option ‘-Wno-format-truncation’ with lower gcc 7
USR_CFLAGS   += -Wno-format-truncation -O3 -std=c99 -g -Wall -mtune=generic -m64 -fPIC -fopenmp -DMKL_ALIGN
USR_CXXFLAGS += -std=c++11

EXCLUDE_ARCHS = linux-ppc64e6500


APP := .
APPDB := $(APP)/db
APPSRC := $(APP)/src

HEADERS += $(wildcard $(APPSRC)/*.h)
SOURCES += $(wildcard $(APPSRC)/*.cpp)
SOURCES += $(wildcard $(APPSRC)/*.c)
SOURCES += $(wildcard $(APPSRC)/*.stt)

DBDS += $(APPSRC)/sis8300llrf.dbd

############################################################################
#
# Add any startup scripts that should be installed in the base directory
#
############################################################################

SCRIPTS += $(wildcard iocsh/*.iocsh)


TEMPLATES += $(wildcard $(APPDB)/*.template)
TEMPLATES += $(APPDB)/sis8300llrf.db
TEMPLATES += $(APPDB)/sp-calib-sel.db

## SYSTEM LIBS
##

ifeq (linux-ppc64e6500, $(findstring linux-ppc64e6500,$(T_A)))
  USR_INCLUDES += -I$(SDKTARGETSYSROOT)/usr/include/libxml2
  #USR_INCLUDES += -I$(SDKTARGETSYSROOT)/usr/include/boost
else ifeq (linux-corei7-poky, $(findstring linux-corei7-poky,$(T_A)))
  USR_INCLUDES += -I$(SDKTARGETSYSROOT)/usr/include/libxml2
  #USR_INCLUDES += -I$(SDKTARGETSYSROOT)/usr/include/boost
else
  USR_INCLUDES += -I/usr/include/libxml2
  #USR_INCLUDES += -I/usr/include/boost
endif

ifneq (linux-corei7-poky, $(findstring linux-corei7-poky,$(T_A)))
    USR_LIBS += gomp
endif

VENDOR_LIBS += ../support/lib/$(T_A)/libmkl_rt.so
VENDOR_LIBS += ../support/lib/$(T_A)/libmkl_rt.so.2

LIB_SYS_LIBS += mkl_rt

USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(APPDB)

SUBS=$(wildcard $(APPDB)/*.substitutions)

.PHONY: vlibs
vlibs:
